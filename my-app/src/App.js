import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        {/* <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header> */}
        <a href="#red"><h1 >红色</h1> </a>
        <a href="#yellow"><h1 >黄色</h1></a> 
        <a href="#blue"><h1 >蓝色</h1></a>

        <div className="color-contents" style={{backgroundColor:'red'}} id="red">123</div> 
        <div className="color-contents" style={{backgroundColor:'yellow'}} id="yellow">123</div> 
        <div className="color-contents" style={{backgroundColor:'blue'}} id="blue">123</div> 
      </div>
    );
  }
}

export default App;
